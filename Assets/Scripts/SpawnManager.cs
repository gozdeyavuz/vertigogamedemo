﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public enum PlayerTeam
{
    None,
    BlueTeam,
    RedTeam
}

public class SpawnManager : MonoBehaviour
{
    [SerializeField] private List<SpawnPoint> _sharedSpawnPoints = new List<SpawnPoint>();
    System.Random _random = new System.Random();
    float _closestDistance;
    [Tooltip("This will be used to calculate the second filter where algorithm looks for closest friends, if the friends are away from this value, they will be ignored")]
    [SerializeField]
    private float _maxDistanceToClosestFriend = 30.0f;
    [Tooltip("This will be used to calculate the first filter where algorithm looks for enemies that are far away from this value. Only enemies which are away from this value will be calculated.")]
    [SerializeField]
    private float _minDistanceToClosestEnemy = 11.0f;
    [Tooltip("This value is to prevent friendly player spawning on top of eachothers. If a player is within the range of this value to a spawn point, that spawn point will be ignored")]
    [SerializeField]
    private float _minMemberDistance = 2.0f;


    public DummyPlayer PlayerToBeSpawned;
    public DummyPlayer[] DummyPlayers;
    float playerDistanceToSpawnPoint;

    private void Awake()
    {
        _sharedSpawnPoints.AddRange(FindObjectsOfType<SpawnPoint>());

        DummyPlayers = FindObjectsOfType<DummyPlayer>();
    }

    #region SPAWN ALGORITHM
    public SpawnPoint GetSharedSpawnPoint(PlayerTeam team)
    {
        List<SpawnPoint> spawnPoints = new List<SpawnPoint>(_sharedSpawnPoints.Count);
        CalculateDistancesForSpawnPoints(team);
        spawnPoints = GetSpawnPointsByDistanceSpawning( ref spawnPoints);
        if (spawnPoints.Count <= 0)
        {
            GetSpawnPointsBySquadSpawning(team, ref spawnPoints);
        }
        SpawnPoint spawnPoint = spawnPoints.Count <= 1 ? spawnPoints[0] : spawnPoints[_random.Next(0, (int)((float)spawnPoints.Count * .5f))];
        spawnPoint.StartTimer();
        return spawnPoint;
    }

    /// <summary>
    /// Yaratılan spawn noktalarını yeni bir liste atar. 
    /// Spawn noktalarının düşmanlara ve dostlara olan mesafelerini hesaplar. 
    /// Mesafeye göre spawn noktalarını seçer, seçemedeği taktirde ekibe göre seçimi yapar. 
    /// </summary>
   

    private List<SpawnPoint> GetSpawnPointsByDistanceSpawning(ref List<SpawnPoint> _suitableSpawnPoints)
    {

    
        if (_suitableSpawnPoints == null)
        {
            _suitableSpawnPoints = new List<SpawnPoint>();
        }
        _suitableSpawnPoints.Clear();

     

        foreach (var x in _sharedSpawnPoints)
        {
            if (x.DistanceToClosestEnemy >= _minDistanceToClosestEnemy && x.DistanceToClosestEnemy > _minMemberDistance && x.DistanceToClosestFriend > _minMemberDistance && x.SpawnTimer <= 0)
            {

                _suitableSpawnPoints.Add(x);
             
            }

        }



        _suitableSpawnPoints.Sort(delegate (SpawnPoint a, SpawnPoint b)
        {
            return a.DistanceToClosestEnemy.CompareTo(b.DistanceToClosestEnemy);
        });


        return _suitableSpawnPoints;

       
    }
    
    ///<summary>
    /// Spawn yapılacabilek uygun noktaları verilen gereksinimlere göre belirler. 
    /// En uygun spawn noktalarını düşmana olan mesafeye göre artan sırada döndürür. 
    /// </summary>





    private void GetSpawnPointsBySquadSpawning(PlayerTeam team, ref List<SpawnPoint> suitableSpawnPoints)
    {
        if (suitableSpawnPoints == null)
        {
            suitableSpawnPoints = new List<SpawnPoint>();
        }
        suitableSpawnPoints.Clear();
        _sharedSpawnPoints.Sort(delegate (SpawnPoint a, SpawnPoint b)
        {
            if (a.DistanceToClosestFriend == b.DistanceToClosestFriend)
            {
                return 0;
            }
            if (a.DistanceToClosestFriend > b.DistanceToClosestFriend)
            {
                return 1;
            }
            return -1;
        });
        for (int i = 0; i < _sharedSpawnPoints.Count && _sharedSpawnPoints[i].DistanceToClosestFriend <= _maxDistanceToClosestFriend; i++)
        {
            if (!(_sharedSpawnPoints[i].DistanceToClosestFriend <= _minMemberDistance) && !(_sharedSpawnPoints[i].DistanceToClosestEnemy <= _minMemberDistance) && _sharedSpawnPoints[i].SpawnTimer <= 0)
            {
                suitableSpawnPoints.Add(_sharedSpawnPoints[i]);
                //Debug.Log(_sharedSpawnPoints[i].DistanceToClosestFriend);
            }
        }
        if (suitableSpawnPoints.Count <= 0)
        {
            suitableSpawnPoints.Add(_sharedSpawnPoints[0]);
        }

    }

    private void CalculateDistancesForSpawnPoints(PlayerTeam playerTeam)
    {
        for (int i = 0; i < _sharedSpawnPoints.Count; i++)
        {
            
            _sharedSpawnPoints[i].DistanceToClosestFriend = GetDistanceToClosestMember(_sharedSpawnPoints[i].PointTransform.position, playerTeam);
            

            _sharedSpawnPoints[i].DistanceToClosestEnemy = GetDistanceToClosestMember(_sharedSpawnPoints[i].PointTransform.position, playerTeam == PlayerTeam.BlueTeam ? PlayerTeam.RedTeam : playerTeam == PlayerTeam.RedTeam ? PlayerTeam.BlueTeam : PlayerTeam.None);
        }

    }
    ///<summary>
    /// Test oyuncusunun spawn noktalarına en yakın düşmanı ve dostlarını hesaplar.
    /// </summary>


    private float GetDistanceToClosestMember(Vector3 position, PlayerTeam playerTeam)
    {

        foreach (var player in DummyPlayers)
        {

            if (!player.Disabled && player.PlayerTeamValue != PlayerTeam.None && player.PlayerTeamValue == playerTeam && !player.IsDead())
            {
                playerDistanceToSpawnPoint = Vector3.Distance(position, player.Transform.position);

               
                
                if (playerDistanceToSpawnPoint < _closestDistance)
                {
                    _closestDistance = playerDistanceToSpawnPoint;

                    return _closestDistance;

                }


                return playerDistanceToSpawnPoint; 
            }
        }

        return -1;

        /*Return değerleri düzenlendi.*/

    }
    
    /// <summary>
    /// Oyuncuların her bir spawn noktasına olan mesafesini hesaplar.
    /// </summary>



    #endregion
    /// <summary>
    /// Test için paylaşımlı spawn noktalarından en uygun olanını seçer.
    /// Test oyuncusunun pozisyonunu seçilen spawn noktasına atar.
    /// </summary>
    public void TestGetSpawnPoint()
    {
        SpawnPoint spawnPoint = GetSharedSpawnPoint(PlayerToBeSpawned.PlayerTeamValue);
        PlayerToBeSpawned.Transform.position = spawnPoint.PointTransform.position;

    }

    ///<summary>
    ///Spawn noktasında test oyuncusunu taşır.
    /// </summary>



}


